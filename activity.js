db.users.insertMany([
		{
			"firstName": "Zero",
			"lastName": "Ultraman",
			"email": "zero.light@mail.com",
			"password": "zero_1234",
			"isAdmin": false
		},
		{
			"firstName": "Cosmos",
			"lastName": "Ultraman",
			"email": "cosmos_ultra@mail.com",
			"password": "universalC20",
			"isAdmin": false
		},
		{
			"firstName": "Mebius",
			"lastName": "Ultraman",
			"email": "mebius1@mail.com",
			"password": "inHerited4",
			"isAdmin": false
		},
		{
			"firstName": "Trigger",
			"lastName": "Ultraman",
			"email": "trigger_eternity@mail.com",
			"password": "4Alpha",
			"isAdmin": false
		},
		{
			"firstName": "Tiga",
			"lastName": "Ultraman",
			"email": "tigashinultra@mail.com",
			"password": "tigaDynamus2020",
			"isAdmin": false
		}

])

db.courses.insertMany([
		{
			"name": "Machine E-learning",
			"price": 2500,
			"isActive":false
		},
		{
			"name": "Phython for Beginners",
			"price": 3800,
			"isActive":false
		},
		{
			"name": "Github Advance",
			"price": 2000,
			"isActive": false
		}


])

db.users.find({"isAdmin":"false"})

db.users.updateOne({"isAdmin": false}, {$set:{"isAdmin": true}})
db.courses.updateOne({"isActive": false}, {$set:{"isActive": true}})

db.courses.deleteMany({"isActive": false})